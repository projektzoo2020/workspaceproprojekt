//
// Created by david.blecha on 16.05.2020.
//

#include "Zvire.h"

#include "Zvire.h"


void Zvire::zvysUrovenZizne(int okolik) {
    m_urovenZizne = m_urovenZizne + okolik;

    if(m_urovenZizne>=100){
        this->umri();
    }
}

void Zvire::zvysUrovenHladu(int okolik) {
    m_urovenHladu = m_urovenHladu + okolik;
    if(m_urovenHladu>=100){
        this->umri();
    }
}

int Zvire::getUrovenHladu() {
    return m_urovenHladu;
}

int Zvire::getUrovenZizne() {
    return m_urovenZizne;

}

void Zvire::setVelikost(int hodnota) {
    m_velikost = hodnota;
}

int Zvire::getVelikost() {
    return m_velikost;
}

std::string Zvire::getZnacka() {
    if(jeMrtve()){
        return "M";
    } else {
        return m_znacka;
    }
}

void Zvire::setZnacka(std::string znacka) {
    m_znacka = znacka;
}

void Zvire::umri(){
    m_velikost = 0;
}

bool Zvire::jeMrtve() {
    return m_velikost <= 0;
}


void Zvire::setPosunuti(bool hodnota) {
    m_posunuti = hodnota;
}

bool Zvire::getPosunuti() {
    return m_posunuti;
}