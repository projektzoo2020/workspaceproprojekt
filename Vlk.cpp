//
// Created by david.blecha on 17.05.2020.
//

#include "Vlk.h"

Vlk::Vlk(int dravost, int velikost){
    m_miraDravosti = dravost;
    m_velikost = velikost;
    m_znacka = "V ";
    m_urovenHladu = 0;
    m_urovenZizne = 0;
    m_identifikator = "dravec";
}

void Vlk::printInfo() {
    std::cout << "Zvire: Vlk" << std::endl;
    std::cout << "Dravost: "<< m_miraDravosti << std::endl;
    std::cout << "Uroven hladu: "<< getUrovenHladu() << std::endl;
    std::cout << "Uroven zizne: "<< getUrovenZizne() << std::endl;
    std::cout << "Velikost: "<< m_velikost << std::endl;
}