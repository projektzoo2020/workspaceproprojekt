//
// Created by tomas on 17.05.2020.
//

#ifndef PROJEKTDEJV_MAPA_H
#define PROJEKTDEJV_MAPA_H

#include <iostream>
#include <array>
#include <iostream>
#include "Zvire.h"
#include <vector>
#include <string>
#include <algorithm>

class Policko;

class Mapa {
private:
    std::array<std::array<Policko*,5>,5> m_mapa;

public:
    Mapa();
    void vykresliMapu();
    Policko* vratPolicko(int x, int y);
    void setPolickuPolickaVOkoli(int x, int y);
    void priradVsemPolickamSousedy();
    void pohniSeVsemiZviraty();
    void vypisInfoOPolicku(int x, int y);
    void pridejZvire(int x, int y, Zvire* zvire);

};


#endif //PROJEKTDEJV_MAPA_H
