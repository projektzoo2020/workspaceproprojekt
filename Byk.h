//
// Created by david.blecha on 17.05.2020.
//

#ifndef PROJEKTDEJV_BYK_H
#define PROJEKTDEJV_BYK_H

#include "Bylozravec.h"

class Byk : public Bylozravec {
public:
    Byk(int inteligece, int velikost);
    void printInfo();
};


#endif //PROJEKTDEJV_BYK_H
