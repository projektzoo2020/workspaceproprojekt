//
// Created by tomas on 04.06.2020.
//

#ifndef OKOLNIPOLICKA_MENU_H
#define OKOLNIPOLICKA_MENU_H
#include "Mapa.h"


class Menu {
    Mapa* m_mapa;

public:
    Menu();
    int getRozhnoti();
    void vyberSiMoznost();
    void interaguj(int rozhodnuti);
};


#endif //PROJEKTDEJV_MENU_H
