//
// Created by tomas on 16.05.2020.
//

#include "Policko.h"


Policko::Policko() {
    m_znacka = "- ";

    if(rand()%4 == 0){
        m_zdroj = new Trava;
    } else if(rand()%4 == 1){
        m_zdroj = new Voda;
    } else {
        m_zdroj = nullptr;
    }


}

void Policko::setZnacka(std::string znacka) {
    m_znacka = znacka;
}

std::string Policko::getZnacka() {
    std::string m_znacka = " ";
    if(obsahujeZvirata()){
        for(int i=0; i<m_zvire.size();i++){
            m_znacka += m_zvire.at(i)->getZnacka();
        }
    }
    else {
        m_znacka += "- ";
    }
    return m_znacka;
}

void Policko::pridejPolickoVOkoli(Policko* policko) {
    m_okolniPolicka.push_back(policko);
}

void Policko::vypisPolickaVOkoli() {
    for(int i=0; i<m_okolniPolicka.size(); i++){
        std::cout << m_okolniPolicka.at(i)->getZnacka() << std::endl;
    }
}

void Policko::printInfo() {
    if(m_zdroj== nullptr){
        std::cout << "Zde neni zadny zdroj" << std::endl;
    } else {
        m_zdroj->printInfo();
    }

    if(obsahujeZvirata()){
        for(int i=0; i<m_zvire.size(); i++){
            m_zvire.at(i)->printInfo();
        }
    }
}

void Policko::setZvire(Zvire *zvire) {
    m_zvire.push_back(zvire);
}

void Policko::odeberZdroj() {
    m_zdroj=nullptr;
}

bool Policko::obsahujeZvirata() {
    return m_zvire.size() > 0;
}

Policko * Policko::vyberNahodnePolickoZOkoli() {
    // nahodne vyberu index
    int nahodny_index = rand() % m_okolniPolicka.size();
    // vratim
    return m_okolniPolicka[nahodny_index];
}

void Policko::pridejZvire(Zvire *zvire) {
    m_zvire.push_back(zvire);
}

Zvire * Policko::getZvire(int index) {
    return m_zvire.at(index);
}


void Policko::odeberZvire() {
    m_zvire.pop_back();
}

int Policko::getPocetZviratNaPolicku() {
    return m_zvire.size();

}
