//
// Created by tomas on 16.05.2020.
//

#ifndef PROJEKTDEJV_VODA_H
#define PROJEKTDEJV_VODA_H

#include "Zdroj.h"

class Voda : public Zdroj{

public:
    Voda();
    void printInfo();
};

#endif //PROJEKTDEJV_VODA_H
