//
// Created by david.blecha on 16.05.2020.
//

#include "Dravec.h"

void Dravec::snezZvire(Zvire *zvire) {
    m_urovenHladu += 90;
    m_urovenZizne += 30;
    if (m_urovenHladu > 100){
        m_urovenHladu = 100;
    }

    if(m_urovenZizne > 100){
        m_urovenZizne = 100;
    }

    zvire->umri();
}

