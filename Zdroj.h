//
// Created by tomas on 16.05.2020.
//

#ifndef PROJEKTDEJV_ZDROJ_H
#define PROJEKTDEJV_ZDROJ_H
#include <iostream>

class Zdroj {

protected:
    std::string m_typZdroje;

public:
    virtual void printInfo()=0;
   std::string getTypZdroje();
};


#endif //PROJEKTDEJV_ZDROJ_H
