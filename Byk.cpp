//
// Created by david.blecha on 17.05.2020.
//

#include "Byk.h"

Byk::Byk(int inteligence, int velikost) {
    m_miraInteligence = inteligence;
    m_velikost = velikost;
    m_znacka = "B ";
    m_urovenHladu = 0;
    m_urovenZizne = 0;
    m_identifikator = "bylozravec";
}

void Byk::printInfo() {
    std::cout << "Zvire: Byk" << std::endl;
    std::cout << "Inteligence: "<< m_miraInteligence << std::endl;
    std::cout << "Uroven hladu: "<< getUrovenHladu() << std::endl;
    std::cout << "Uroven zizne: "<< getUrovenZizne() << std::endl;
    std::cout << "Velikost: "<< m_velikost << std::endl;
}
