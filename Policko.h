//
// Created by tomas on 16.05.2020.
//

#ifndef PROJEKTDEJV_POLICKO_H
#define PROJEKTDEJV_POLICKO_H
#include <iostream>
#include <vector>
#include "Zdroj.h"
#include "Trava.h"
#include "Voda.h"
#include "Zvire.h"
#include <math.h>

class Policko {
private:
    std::string m_znacka;
    std::vector<Policko*> m_okolniPolicka;
    Zdroj* m_zdroj;
    std::vector<Zvire*> m_zvire;


public:
    Policko();
    std::string getZnacka();
    void setZnacka(std::string);
    void pridejPolickoVOkoli(Policko* policko);
    void vypisPolickaVOkoli();
    Policko* vyberNahodnePolickoZOkoli();
    void pridejZvire(Zvire* zvire);
    Zvire* getZvire(int index);
    void odeberZvire();
    int getPocetZviratNaPolicku();
    void setZvire(Zvire* zvire);
    void odeberZdroj();
    bool obsahujeZvirata();
    void printInfo();

};


#endif //PROJEKTDEJV_POLICKO_H
