//
// Created by tomas on 04.06.2020.
//

#include "Menu.h"

#include "Menu.h"
#include "Byk.h"
#include "Srnka.h"

Menu::Menu() {
    m_mapa = new Mapa();
    std::cout << "*** Herni mapa byla vytvorena! *** \n";
}

int Menu::getRozhnoti() {
    int input;
    std::cin >> input;
    return input;
}

void Menu::vyberSiMoznost() {
    std::cout << "\n";
    std::cout << " *-*Stiskni jednicku pro ziskani informace o policku.*-*  \n";
    std::cout << "*-*Stiskni dvojku pro zapnuti hry.*-* \n";
    interaguj(getRozhnoti());
}

void Menu::interaguj(int rozhodnuti){
    switch(rozhodnuti){
        case 1:
            int x,y;
            std::cout << "Zadej souradnici x a pak y:";
            std::cin >> x;
            std::cin >> y;
            m_mapa->vypisInfoOPolicku(x,y);
            break;
        case 2:
            std::cout << "Kolik kol chces hrat?";
            int pocetKol;
            std::cin >> pocetKol;

            int x1,y1,x2,y2;
            std::cout << "Zadej souradnice x prvniho zvirete: ";
            std::cin >> x1;
            std::cout << "Zadej souradnice y prvniho zvirete: ";
            std::cin >> y1;
            std::cout << "Zadej souradnice x druheho zvirete: ";
            std::cin >> x2;
            std::cout << "Zadej souradnice y druheho zvirete: ";
            std::cin >> y2;
            Zvire* byk = new Byk(4,4);
            Zvire* srnka = new Srnka(21,5500);

            m_mapa->pridejZvire(x1,y1, byk);
            m_mapa->pridejZvire(x2,y2, srnka);

            for(int i=0; i<pocetKol; i++){
                std::cout << i+1 << ". kolo! \n";
                m_mapa->pohniSeVsemiZviraty();
                m_mapa->vykresliMapu();
            }
    }
}

