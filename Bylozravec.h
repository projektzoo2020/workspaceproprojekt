//
// Created by david.blecha on 16.05.2020.
//

#ifndef OKOLNIPOLICKA_LEV_H
#define OKOLNIPOLICKA_LEV_H

#include "Zvire.h"
#include "Policko.h"

class Bylozravec : public Zvire{
protected:
    int m_miraInteligence;

public:
    void snezTravu(Policko* pole);
    virtual void printInfo()=0;

};



#endif //OKOLNIPOLICKA_LEV_H