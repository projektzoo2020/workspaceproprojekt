//
// Created by tomas on 17.05.2020.
//

#include "Mapa.h"
#include "Policko.h"


Mapa::Mapa() {
    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            m_mapa.at(i).at(j) = new Policko();
        }
    }

    priradVsemPolickamSousedy();
}

void Mapa::vykresliMapu()  {
    for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
            std::cout << m_mapa.at(i).at(j)->getZnacka();
        }
        std::cout << std::endl;
    }
}

Policko* Mapa::vratPolicko(int x, int y) {
    return m_mapa.at(x).at(y);
}

void Mapa::setPolickuPolickaVOkoli(int x, int y) {
    Policko * policko = vratPolicko(x, y);
    if (x+1 <= 4) {
        policko->pridejPolickoVOkoli(vratPolicko(x+1,y));
    }

    if(y+1<=4){
        policko->pridejPolickoVOkoli(vratPolicko(x,y+1));
    }

    if(x-1>=0){
        policko->pridejPolickoVOkoli(vratPolicko(x-1,y));
    }

    if(y-1>=0){
        policko->pridejPolickoVOkoli(vratPolicko(x,y-1));
    }
}

void Mapa::priradVsemPolickamSousedy() {
    for(int i=0; i<m_mapa.size(); i++){
        for(int j=0; j<m_mapa.size(); j++){
            setPolickuPolickaVOkoli(i,j);
        }
    }
}


void Mapa::pohniSeVsemiZviraty() {
    for(int i=0; i<m_mapa.size(); i++){
        for(int j=0; j<m_mapa.size(); j++){

            if(m_mapa.at(i).at(j)->obsahujeZvirata()){


                for(int x=m_mapa.at(i).at(j)->getPocetZviratNaPolicku()-1; x>=0; x--){

                    Zvire* zvire = m_mapa.at(i).at(j)->getZvire(x);
                    if(!zvire->jeMrtve() & !zvire->getPosunuti()){
                        Policko* randomPolicko = m_mapa.at(i).at(j)->vyberNahodnePolickoZOkoli();
                        zvire->zvysUrovenHladu(15);
                        zvire->zvysUrovenZizne(15);
                        randomPolicko->pridejZvire(zvire);
                        zvire->setPosunuti(1);
                        m_mapa.at(i).at(j)->odeberZvire();

                    }
                }


            }
        }
    }
        for(int i=0; i<m_mapa.size(); i++){
            for(int j=0; j<m_mapa.size(); j++){
                if(m_mapa.at(i).at(j)->obsahujeZvirata()){
                    for(int y=0; y<m_mapa.at(i).at(j)->getPocetZviratNaPolicku(); y++){
                    Zvire* zvire = m_mapa.at(i).at(j)->getZvire(y);
                    zvire->setPosunuti(0);
                }
            }
        }
    }
}


void Mapa::vypisInfoOPolicku(int x, int y) {
    m_mapa.at(x).at(y)->printInfo();
}


void Mapa::pridejZvire(int x, int y, Zvire *zvire) {
    m_mapa.at(x).at(y)->pridejZvire(zvire);
}
