//
// Created by david.blecha on 16.05.2020.
//

#ifndef PROJEKTDEJV_DRAVEC_H
#define PROJEKTDEJV_DRAVEC_H

#include "Zvire.h"

class Dravec : public Zvire {
protected:
    int m_miraDravosti;

public:
    virtual void snezZvire(Zvire* zvire);
    virtual void printInfo()=0;

};



#endif //PROJEKTDEJV_DRAVEC_H
