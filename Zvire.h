//
// Created by david.blecha on 16.05.2020.
//

#ifndef PROJEKTDEJV_ZVIRE_H
#define PROJEKTDEJV_ZVIRE_H
#include <iostream>

class Zvire {
protected:
    int m_velikost;
    int m_urovenHladu;
    int m_urovenZizne;
    std::string m_znacka;
    bool m_posunuti;
    std::string m_identifikator;

public:
    void zvysUrovenZizne(int okolik);
    void zvysUrovenHladu(int okolik);
    int getUrovenHladu();
    int getUrovenZizne();
    void setVelikost(int hodnota);
    int getVelikost();
    std::string getZnacka();
    void setZnacka(std::string znacka);
    void umri();
    virtual void printInfo()=0;
    bool jeMrtve();
    void setPosunuti(bool hodnota);
    bool getPosunuti();
};


#endif //PROJEKTDEJV_ZVIRE_H
