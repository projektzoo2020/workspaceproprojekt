//
// Created by david.blecha on 17.05.2020.
//

#ifndef PROJEKTDEJV_LEV_H
#define PROJEKTDEJV_LEV_H

#include "Dravec.h"

class Lev : public Dravec {
public:
    void printInfo();
    Lev(int dravost, int velikost );

};


#endif //PROJEKTDEJV_LEV_H
