//
// Created by david.blecha on 17.05.2020.
//

#ifndef PROJEKTDEJV_SRNKA_H
#define PROJEKTDEJV_SRNKA_H

#include "Bylozravec.h"

class Srnka : public Bylozravec{
public:
    Srnka(int inteligence, int velikost);
    void printInfo();
};


#endif //PROJEKTDEJV_SRNKA_H
