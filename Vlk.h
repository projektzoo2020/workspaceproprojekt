//
// Created by david.blecha on 17.05.2020.
//

#ifndef PROJEKTDEJV_VLK_H
#define PROJEKTDEJV_VLK_H

#include "Dravec.h"

    class Vlk : public Dravec{
    public:
        void printInfo();
        Vlk(int dravost, int velikost);
    };


#endif //PROJEKTDEJV_VLK_H
