//
// Created by david.blecha on 17.05.2020.
//

#include "Lev.h"

Lev::Lev(int dravost, int velikost){
    m_miraDravosti = dravost;
    m_velikost = velikost;
    m_znacka = "L ";
    m_urovenHladu = 0;
    m_urovenZizne = 0;
    m_identifikator = "dravec";
}

void Lev::printInfo() {
    std::cout << "Zvire: Lev" << std::endl;
    std::cout << "Dravost: "<< m_miraDravosti << std::endl;
    std::cout << "Uroven hladu: "<< getUrovenHladu() << std::endl;
    std::cout << "Uroven zizne: "<< getUrovenZizne() << std::endl;
    std::cout << "Velikost: "<< m_velikost << std::endl;
}